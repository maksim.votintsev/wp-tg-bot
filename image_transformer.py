import os
import json
import cv2
import numpy as np
import logging
import time


class ImageTransformer:
    """ImageTransformer is an entity that can be used to transform images to Instagram Story format
    """
    def __init__(self, log: bool = False) -> None:
        """All the parameters load from image_transformer_config.json
        For now it supports: output_size, padding, background_color
        """
        self.__logger = logging.getLogger("ImageTransformer")
        try:
            with open('image_transformer_config.json', 'r') as config_file:
                config = json.load(config_file)

                self.padding = config['padding']
                if not isinstance(self.padding, int):             
                    self.__logger.error(f'Invalid padding. Must be int, got {type(self.padding)}')
                    raise ValueError('Padding must be an integer')

                self.output_size = config['output_size']
                if not isinstance(self.output_size, list) and len(self.output_size) != 2:
                    self.__logger.error(f'Invalid output_size. Must be list and have len 2, \
                        got {type(self.output_size)}, len {len(self.output_size)}')
                    raise ValueError('Output_size must be list and have len 2')

                self.background_color = config['background_color']
                # !!!TODO: Pleeease!! Pomanyaite 'background_color', chtobi on vvodil v RGB cvet, a ne v BGR. Breeeeeead!))))
                if not isinstance(self.background_color, list) and len(self.background_color) != 3:
                    self.__logger.error(f'Invalid background_color. Must be list and have len 3, \
                        got {type(self.background_color)}, len {len(self.background_color)}')
                    raise ValueError('background_color must be list and have len 3')
                self.__logger.info("Config was loaded successfully")
        except json.decoder.JSONDecodeError as e:
            self.__logger.error("Couldn't decode config file")
            self.__logger.error(e)
            raise e
        except KeyError as e:
            self.__logger.error("Trying to read a key that doesn't exist in config file")
            self.__logger.error(e)
            raise e
        except FileNotFoundError as e:
            self.__logger.error('Config file "image_transformer_config.json" not found')
            self.__logger.error(e)
            raise e
        self._image = None
        self._image_name = None

    def load_image(self, image_path: str) -> np.array:
        """Loads an image to an opencv format (np.array).

        Args:
            image_path (str): Path to an image to load. Example: /path/to/an/image.extension

        Returns:
            np.array: resulted image after loading
        """
        self.__logger = logging.getLogger("ImageTransformer.load_image")
        if not isinstance(image_path, str):
            self.__logger.error(f"Image path must be a string, got {type(image_path)}")
            raise ValueError('Image path must be a string. Example: "path/to/an/image.extension"')
        try:
            self._image = cv2.imread(image_path)
            self.__logger.info('Image was loaded successfully')
        except Exception as e:
            self.__logger.error("Error while reading image")
            self.__logger.error(e)
            raise e
        self._image_name = image_path.split('/')[-1]
        self.__logger.info(f'Image was given a name {self._image_name}')
        return self._image

    def save_image(self, image: np.array = None, savedir: str = 'default') -> None:
        """Saves given or inner image to the specified directory. 
        Uses the name that image had before or generates it from time.

        Args:
            image (np.array, optional): Image to save. If not specified, the inner image of the class will be saved
            savedir (str, optional): [description]. Path, where to save the image. Default is "result" directory
        """
        self.__logger = logging.getLogger("ImageTransformer.save_image")
        if savedir == 'default':
            if not os.path.exists('result'):
                os.makedirs('result')
            savedir = os.path.join('result')
        else:
            savedir = os.path.normpath(savedir)
            if not os.path.exists(savedir):
                self.__logger.warning(f"Directory {savedir} does not exist")
                try:
                    self.__logger.warning(f"Trying to create directory {savedir}")
                    os.makedirs(savedir)
                except PermissionError:
                    self.__logger.warning(f"Permission denied to create directory {savedir}")
                    self.__logger.warning('Saving to "result" directory')
                    savedir = os.path.join('result')
        if self._image_name is None:
            self._image_name = str(time.time())
        if image is None:
            cv2.imwrite(os.path.join(savedir, self._image_name), self._image)
        else:
            if not isinstance(image, np.array):
                self.__logger.error(f'image must be a np.array, got {type(image)}')
                raise ValueError('image must be a np.array')
            cv2.imwrite(os.path.join(savedir, self._image_name), image)
        self.__logger.info(f'Image was saved successfully to the destination: \
            {os.path.join(savedir, self._image_name)}')

    def view_image(self, image=None, window_name: str = 'default') -> None:
        """use it to view an opencv image or an inner image

        Args:
            image (np.array, optional): Image to show. If not specified, the inner image of the class will be shown.
            window_name (str, optional): Use it, if you want to specify window name.
        """
        self.__logger = logging.getLogger("ImageTransformer.view_image")
        if window_name == 'default':
            window_name = self._image_name
        if not isinstance(window_name, str):
            self.__logger.error(f'window_name must be string, got {type(window_name)}')
            raise ValueError('window_name must be string')
        cv2.namedWindow(window_name, cv2.WINDOW_NORMAL)
        if image is None:
            cv2.imshow(window_name, self._image)
        else:
            if not isinstance(image, np.array):
                self.__logger.error(f'image must be a np.array, got {type(image)}')
                raise ValueError('image must be a np.array')
            cv2.imshow(window_name, image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    def __is_album_oriented(self):
        # if Width bigger or evolve then Height: 
        #   return  Album orient
        # else: return Portrait orient
        # Source: https://stackoverflow.com/questions/19098104/python-opencv2-cv2-wrapper-to-get-image-size
        # return Width >= Height
        return self._image.shape[1] >= self._image.shape[0]

    def resize(self) -> np.array:
        """Resizes image to fit Instagram story format.
        Uses output_size and padding from config.

        Returns:
            np.array: resulted image after resize
        """
        self.__logger = logging.getLogger("ImageTransformer.resize")
        if self.__is_album_oriented():
            self.__logger.info("Image is horisontally oriented")
            resulted_width = self.output_size[0] - 2 * self.padding
            scale_ratio = resulted_width / self._image.shape[1]
            resulted_height = int(self._image.shape[0] * scale_ratio)
        else:
            self.__logger.info("Image is vertically oriented")
            resulted_height = self.output_size[1] - 2 * self.padding
            scale_ratio = resulted_height / self._image.shape[0]
            resulted_width = int(self._image.shape[1] * scale_ratio)

        # HOTFIX !TODO: sdelai, please, po logiс'E)))
        if resulted_width >= (self.output_size[0] - 2 * self.padding):
            self.__logger.warning("Fix by Width")
            resulted_width = self.output_size[0] - 2 * self.padding
            scale_ratio = resulted_width / self._image.shape[1]
            resulted_height = int(self._image.shape[0] * scale_ratio)
        elif resulted_height >= (self.output_size[1] - 2 * self.padding):
            self.__logger.warning("Fix by Height")
            resulted_height = self.output_size[1] - 2 * self.padding
            scale_ratio = resulted_height / self._image.shape[0]
            resulted_width = int(self._image.shape[1] * scale_ratio)
        # HOTFIX end.


        resulted_size = (resulted_width, resulted_height)
        result = cv2.resize(self._image, resulted_size, interpolation=cv2.INTER_AREA)
        self.__logger.info(f"Image size before conversion: \
            {(self._image.shape[1], self._image.shape[0])}")
        self._image = result
        self.__logger.info(f"Image size after conversion: \
            {(self._image.shape[1], self._image.shape[0])}")
        return self._image

    def add_background(self) -> np.array:
        """Adds background to the image. Uses background_color from config.
        Use it after resize only!

        Returns:
            np.array: resulted image after adding background.
        """
        self.__logger = logging.getLogger("ImageTransformer.add_background")
        background = np.zeros([self.output_size[1], self.output_size[0], 3], dtype=np.uint8)
        for channel, channel_color in enumerate(self.background_color):
            background[:, :, channel].fill(channel_color)
        self.__logger.info(f"Using {self.background_color} color for the background")
        try:
            background[(self.output_size[1] - self._image.shape[0]) // 2:
                    (self.output_size[1] + self._image.shape[0]) // 2,
                    (self.output_size[0] - self._image.shape[1]) // 2:
                    (self.output_size[0] + self._image.shape[1]) // 2] = \
                self._image
        except ValueError as e:
            self.__logger.error("Error while trying to combine background and image")
            self.__logger.error(e)
        self._image = background
        return self._image

    def transform_image(self, image=None, image_path: str = None):
        assert not ((image is not None) and (image_path is not None)), \
            "Image or image_path should be specified"
        if image is not None:
            self._image = image
        elif image_path is not None:
            self.load_image(image_path)
        self.resize()
        self.add_background()
        return self._image


if __name__ == '__main__':
    it = ImageTransformer(log=True)
    it.load_image('sample_5.jpg')
    it.transform_image()
    it.view_image()
    it.save_image()

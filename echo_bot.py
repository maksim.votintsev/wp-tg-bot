import telebot
# For MIME types
import magic # DOCs: https://github.com/ahupp/python-magic
import time
import re

import os
from transliterate import translit # DOCs: https://pypi.org/project/transliterate/

import requests
import json

from image_transformer import ImageTransformer 

TOKEN = '1517554123:AAHKZM3dFkrwmb3DY8udLHnxrctEMsfUMqs'
bot = telebot.TeleBot(TOKEN)

keyboard1 = telebot.types.ReplyKeyboardMarkup(True, True)
keyboard1.row('Привет', 'Пока')

# tconv = lambda x: time.strftime("")

@bot.message_handler(commands=['stop'])
def start_message(message):
    bot.send_message(message.chat.id, 'Привет, ты написал мне /stop')

@bot.message_handler(commands=['start'])
def start_message(message):
    print(message.date)
    bot.send_message(message.chat.id, 'Привет, ты написал мне /start', reply_markup=keyboard1)

@bot.message_handler(content_types=["photo"])
def echo_all(message):
    # try:
    '''
        !!!первая проверка: на наличие заголовка!
    '''
    if (message.caption == None):
        bot.reply_to(message, '❗️❗️❗️Пост НЕ опубликован. Добавьте хотя бы заголовок!')
        return    
    '''
        !!!вторая проверка: на правильность ввода даты в конце текста!
    '''
    content_parsed = str(message.caption).split('\n')
    # print(content_parsed)
    # print(len(content_parsed))
    if (len(content_parsed) > 1):
        try:
            time_string = content_parsed[-1]
            print(time_string)
            result_time = time.strptime(time_string, "%H:%M %d-%m-%Y")
            print(result_time)
            # date_time = time.strftime("%Y-%m-%dT%H:%M:00", result_time)
            # print(date_time)
        except BaseException: 
            try: 
                # temp_var = time.strptime(time_string, "%H:*")
                temp_var = re.search(r'\d\d:.*', time_string)
                # print(temp_var)
                if temp_var:
                    bot.reply_to(message, '❗️❗️❗️Пост НЕ будет опубликован. Попробуйте исправить дату в формате 14:52 14-03-2021 и снова отослать пост боту')
                    return
                else:
                    print('Проверка на время пройдена')
            except BaseException: 
                print("Мы попали. Время остаётся таким же")
    # return
    # best practice: добавить это в функцию
    file_info = bot.get_file(message.photo[-1].file_id)
    image_file = file_info.file_path.replace("/", "_")
    print(image_file)
    response = requests.get('https://api.telegram.org/file/bot{0}/{1}'.format(TOKEN, file_info.file_path))
    print(file_info.file_path)
    first_image_name = image_file
    image_file = '/tmp/' + image_file
    with open(image_file, 'wb') as f:
        f.write(response.content)
    base = os.path.basename(image_file)
    abs_path = os.path.abspath(image_file)
    print(abs_path)
    print(base)

    it = ImageTransformer(log=True)
    it.load_image(abs_path)
    it.transform_image()
    # it.view_image()
    it.save_image(savedir='/tmp/transformed/')
    # it's correct, it's WORK
    image_file = '/tmp/transformed/' + first_image_name
    base = os.path.basename(image_file)
    abs_path = os.path.abspath(image_file)
    print(abs_path)
    print(base)

    mime = magic.Magic(mime=True)
    print("File MIME type: ")
    print( mime.from_file(image_file) )
    mime = mime.from_file(image_file)
    # print(mime.split("/", maxsplit=1)[0])

    response_image = '' # 1. Переменная для ответа по картинке
    if mime.split("/", maxsplit=1)[0] == "image":
        # запуск bash
#             bashCommand = """curl --location \\
# --request POST "http://чежевика.рф/wp-json/wp/v2/media" \\
# --header "Content-Disposition: attachment; filename=""" + base + """" \\
# --header "Authorization: Basic YWRtaW46QGdWS0plXjEyMw==" \\
# --header "Content-Type: """ + mime + """" \\
# --data-binary "@""" + abs_path + """" """
        # print(bashCommand)
        # os.system(bashCommand)
        # !!!переписать под requests
        url = "http://чежевика.рф/wp-json/wp/v2/media"

        headers = {
            'Content-Disposition': 'attachment; filename='+base,
            'Authorization': 'Basic YWRtaW46QGdWS0plXjEyMw==',
            'Content-Type': mime
        }
        # print(headers)
        with open(abs_path, 'rb') as f:
            response_image = requests.request("POST", url, headers=headers, data=f)

            # print(response_image.text)
    else:
        bot.reply_to(message, "ФУСК with MIME-type of file. Проще говоря ваш файл не картинка((")

    # 2. Постинг контента
    if response_image.status_code == 201:           
        bot.reply_to(message, 'Картинка загружена на ресурс!! Проверьте в разделе медиа на чежевика.рф!\n\nНачинаем готовить пост...')

        content_parsed = str(message.caption).split('\n')
        # print(str(message.caption).split('🚀')[1].split('🤙')[0].split('\n')[0])
        # print(str(message.caption).split('🤙')[1])

        resp2 = json.loads(response_image.text)
        
        title=''
        content=''
        date_time = '' # time.strftime("%Y-%m-%dT%H:%M:00", time.localtime(message.date))
        # нет необходимости указывать дату из сообщения. Если даты нет, то публикация мгновенна :)
        if (len(content_parsed) <= 1):
            title = content_parsed[0]
            print(title)
            # print(date_time)
            bot.reply_to(message, 'Пост опубликуется мгновенно.')
        else:
            title = content_parsed[0]
            time_string = content_parsed[-1]
            content = content_parsed[1:-1]
            print(title)
            print(time_string)
            try:
                result_time = time.strptime(time_string, "%H:%M %d-%m-%Y")
                time.altzone = 3
                print(time.mktime(result_time))
                print(message.date)
                print('struct_time:')
                print(result_time)
                print(time.gmtime(message.date))
                if result_time > time.gmtime(message.date): # планируемая дата должна быть превышать дату сообщения
                    # иначе пост опубликуется в прошлое
                    # BUG!! Время бота отличается от вводимой на UTC. Надо как-то брать в UTC, потому что
                    # время ты вводишь по своему часовому поясу
                    print(result_time)
                    date_time = time.strftime("%Y-%m-%dT%H:%M:00", result_time)
                    bot.reply_to(message, 'Пост опубликуется на запланированное время '+time_string+'.')
                else:
                    bot.reply_to(message, 'Пост опубликуется мгновенно, так как дата и время были указаны в прошлое.')
                print(date_time)
            except BaseException: 
                try: 
                    # temp_var = time.strptime(time_string, "%H:*")
                    # print(temp_var)
                    if re.search(r'\d\d:.*', time_string):
                        bot.reply_to(message, '❗️❗️❗️Пост НЕ будет опубликован. Попробуйте исправить дату в формате 14:52 14-03-2021 и снова отослать пост боту')
                        return
                    else:
                        content.append(time_string)
                        bot.reply_to(message, 'Пост опубликуется мгновенно.')
                except BaseException: 
                    print("Мы попали. Время остаётся таким же")
                    content.append(time_string)
                    bot.reply_to(message, 'Пост опубликуется мгновенно.')
            for i in range(0, len(content)):
                # print(item)
                content[i] = "<p>"+content[i]+"</p>"
            # print(content)
            content = '\n'.join(content)
            # print(content)

        url = "http://чежевика.рф/wp-json/wp/v2/posts"

        # date_time = "2021-03-07T14:52:00" # время + дата ЧЧ:мм ДД-ММ-ГГГГ
        # title = "Пост в будущее"
        # content = "<p>Контент для поста</p>\n<p>C продолжением</p>\n"
        payload=''
        if date_time=='':
            payload={
                "status": "future",
                "title": title,
                "content": content,
                "featured_media": resp2['id'],
                "format": "standard"
            }
        else:
            payload={
                "date": date_time,
                "status": "future",
                "title": title,
                "content": content,
                "featured_media": resp2['id'],
                "format": "standard"
            }
        headers = {
            'Authorization': 'Basic YWRtaW46QGdWS0plXjEyMw==',
            'Content-Type': 'application/json'
        }
        print(payload)
        # return # CHECK THIS!!

        response_post = requests.request("POST", url, headers=headers, json=payload)
        if response_image.status_code == 201:
            bot.reply_to(message, 'От сайта пришёл положительный ответ по посту. Можно проверить на чежевика.рф его наличие')
        else:
            bot.reply_to(message, 'Что-то в самом конце пошло не так')
            print(response_post.text)
    else:
        bot.reply_to(message, 'Картинка не загружена((')
    # except BaseException:
    #     bot.reply_to(message, 'Какая-то фигня с обработкой сообщения. Обратитесь к админу БОТа за поддержкой😊')

@bot.message_handler(content_types=['text'])
def send_text(message):
    if message.text.lower() == 'привет':
        bot.send_message(message.chat.id, 'Привет, мой создатель')
    elif message.text.lower() == 'пока':
        bot.send_message(message.chat.id, 'Прощай, создатель')
    elif message.text.lower() == 'я тебя люблю':
        bot.send_sticker(message.chat.id, 'CAADAgADZgkAAnlc4gmfCor5YbYYRAI')

bot.polling()